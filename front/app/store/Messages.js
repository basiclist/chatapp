Ext.define('test.store.Messages', {
    extend: 'Ext.data.Store',
    storeId: 'messages',
    alias: 'store.messages',
    model: 'test.model.Message',
    //autoLoad: true,
    proxy: {
        type: 'ajax',
        url: '/get_messages',
        reader: {
            type: 'json',
            //keepRawData: true,
            //rootProperty: 'items'
        }
    }
});
