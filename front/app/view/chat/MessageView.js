Ext.define('test.view.chat.MessageView', {
    extend: 'Ext.view.View',
    requires: ['test.store.Messages'],
    xtype: 'chatmesview',
    scrollable: true,
    tpl: new Ext.XTemplate(
        '<div class="chat">',
            '<tpl for=".">',
                '<div class="chat-message{postfix}">',
                    '<div class="message-head">',
                        '<span class="userame">{username}</span> ',
                    '</div>',
                    '<div>{message}</div>',
                '</div>',
            '</tpl>',
        '</div>'
    ),
    itemSelector: 'div.thumb-wrap',
    store: Ext.create('test.store.Messages'),
    scrollToBottom: function() {
        Ext.fly(this.getEl().dom).down('.chat').dom.scrollTo(0, 999999);
    }
});
