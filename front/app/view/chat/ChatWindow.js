Ext.define('test.view.chat.ChatWindow', {
    extend: 'Ext.window.Window',
    requires: ['test.view.chat.MessageView'],
    width: 430,
    height: 400,
    referenceHolder: true,
    controller: 'chat',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    listeners: {
        afterrender: 'onChatOpen',
        beforedestroy: 'onChatClose'
    },
    items: [{
        xtype: 'chatmesview',
        margin: 5,
        flex: 3,
        reference: 'message_view'
    }, {
        xtype: 'panel',
        reference: 'message_bottom',
        minHeight: 80,
        margin: '0 0 5 0',
        layout: {
            type: 'hbox',
            align: 'stretchmax'
        },
        items: [{
            xtype: 'textarea',
            flex: 5,
            margin: '0 0 0 5',
            reference: 'chat_message',
            enableKeyEvents: true,
            grow: true,
            growMax: 200,
            listeners: {
                keypress: 'onTextareaKeypress',
                resize: 'onTextareaResize'
            },
        }, {
            xtype: 'button',
            text: 'Отправить',
            reference: 'chat_sendbtn',
            minWidth: 100,
            margin: '0 5 0 5',
            flex: 1,
            handler: 'onSendBtnClick'
        }]
    }],
    load: function() {
        var me = this;
        me.setTitle(me.username);
        var message_view = me.lookupReference('message_view');
        message_view.getStore().load({
            params: {
                username: me.username
            },
            callback: function(){
                message_view.scrollToBottom();
            }
        });
    }
});
