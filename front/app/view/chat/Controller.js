Ext.define('test.view.chat.Controller', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chat',
    requires: [
        'test.view.chat.ChatWindow'
    ],

    onTextareaKeypress: function(field, event) {
        // Ввод символов в поле сообщения чата
        // Обработка Ctrl+Enter
        if (!event.ctrlKey) {
            return true;
        }
        if (event.getKey() == 10) {
            this.onSendBtnClick();
            return false;
        }
        return true;
    },

    onTextareaResize: function(field,  width, height, oldWidth, oldHeight) {
        // Изменение размеров окна вслед за изменением размеров textarea
        var view = this.getView();

        // При инициализации происходит resize с пустым oldHeight
        // Ничего делать не нужно
        if (!oldHeight) {
            return true;
        }
        var delta = height - oldHeight;
        var msgPanel = view.lookupReference('message_bottom');
        var newWinHeight = view.getHeight() + delta;
        var newPanelHeight = msgPanel.getHeight() + delta;
        msgPanel.setSize(msgPanel.getWidth(), newPanelHeight);
        view.setSize(view.getWidth(), newWinHeight);
    },

    onChatOpen: function () {
        // Открытие окна чата
        // Соединяемся с сокетом чата для обмена сообщениями
        // При получении сообщения добавляем его на форму

        var me = this;
        me.socket = io();
        me.socket.on('chat message', function(data){
            me.onGetMessage(data);
        });
    },

    onChatClose: function() {
        this.socket.disconnect();
    },

    onGetMessage: function(data) {
        // При получении сообщения добавляем его на форму
        // Свои сообщения выделяем другим цветом.
        // И скроллим на низ экрана
        var me = this;
        var view = me.getView();
        var msgView = view.lookupReference('message_view');
        var chatStore = msgView.getStore();
        // Суфикс '-my' означает, что сообщение отправлено от текущего пользователя
        var postfix = data.username == view.username ? '-my' : '';
        chatStore.add({'message': data.msg,
                       'username': data.username,
                       'postfix': postfix});
        // Скроллинг на низ экрана
        msgView.scrollToBottom();
    },

    onSendBtnClick: function() {
        // Отправка сообщения на сервер
        var messageFld = this.lookupReference('chat_message');
        var view = this.getView();
        var data = {
            msg: messageFld.getValue(),
            username: view.username
        };
        this.socket.emit('chat message', data);
        messageFld.setValue('');
    }
});
