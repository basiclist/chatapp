Ext.define('test.model.Message', {
    extend: 'Ext.data.Model',
    alias: 'model.message',
    fields: [
        { name: 'user_id', type: 'int' },
        { name: 'username', type: 'string' },
        { name: 'message', type: 'string' },
        { name: 'postfix', type: 'string' }
    ]
});
