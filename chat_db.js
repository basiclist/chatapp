var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('abc.db');

exports.create_db = function() {
	db.serialize(function() {

  	db.run("CREATE TABLE if not exists chat_messages (username TEXT, message TEXT)");
  // var uname = '3';
  // var query = "SELECT rowid AS user_id, username, message," +
  //               " case message " +
  //                   "when '" + uname + "' then '-my' else '' " +
  //               "end as postfix" +
  //             " FROM chat_messages"
  // db.all(query, function(err, row) {
  //   console.log(row);
      // console.log(row.id + ": username " + row.username + "; mess: " + row.message + '; ' + row.postfix);
  // });
	});
};

exports.get_all_messages = function(res, uname) {
  var query = "SELECT rowid AS user_id, username, message," +
                " case username " +
                    "when ? then '-my' else '' " +
                "end as postfix" +
              " FROM chat_messages"
  db.all(query, [uname], function(err, rows) {
    res.json(rows)
  });
};

exports.append_message = function(data) {
	var query = "insert into chat_messages (username, message) values (?, ?)";
	var values = [data.username, data.msg];
	db.run(query, values);
}
