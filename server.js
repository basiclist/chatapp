var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = 5000;
var chat_db = require('./chat_db');

chat_db.create_db();

app.use(express.static('front'));


app.get('/', function(req, res){
  res.sendFile(__dirname + '/front/index.html');
});


app.get('/get_messages', function(req, res){
  console.log(req.query.username);
  chat_db.get_all_messages(res, req.query.username);
});

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('chat message', function(data){
  	console.log(data);
    chat_db.append_message(data);
    io.emit('chat message', data);
  });
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});
